require 'spec_helper'
require 'yaml'

describe "Kubernetes Service Account" do
  before do |needs_sample_data|
    @service_account_a = YAML.load_file(File.expand_path("spec/fixtures/service_account_a.yaml"))
    @service_account_b = YAML.load_file(File.expand_path("spec/fixtures/service_account_b.yaml"))

    @roles = []
    @roles.push(YAML.load_file(File.expand_path("spec/fixtures/role_a.yaml")))
    @roles.push(YAML.load_file(File.expand_path("spec/fixtures/role_b.yaml")))

    @role_bindings = []
    @role_bindings.push(YAML.load_file(File.expand_path("spec/fixtures/bind_role_a_to_account_a.yaml")))
    @role_bindings.push(YAML.load_file(File.expand_path("spec/fixtures/bind_role_b_to_account_b.yaml")))
    @name_mappings = {}
    @name_mappings = YAML.load_file(File.expand_path("spec/fixtures/service_account_mappings.yaml"))
  end

  context "when initialized with no parameter" do
    it "should raise an error" do
      expect { KubernetesSubject.new }.to raise_error(ArgumentError)
    end
  end

  context "when initialized" do
    it "should create an object" do
      expect(KubernetesSubject.new(@service_account_a)).to be_an_instance_of(KubernetesSubject)
    end

    it "two objects from the same data should be equal" do
      a = KubernetesSubject.new @service_account_a
      b = KubernetesSubject.new @service_account_a
      expect(a).to eq(b)
    end

    it "two objects from different data should be unequal" do
      a = KubernetesSubject.new @service_account_a
      b = KubernetesSubject.new @service_account_b
      expect(a).not_to eq(b)
    end
  end

  context "when defining roles" do
    it "should be able to have at least one role" do
      a = KubernetesSubject.new @service_account_a
      a.define_roles @roles, @role_bindings
      expect(a.roles.length).to be > 0
    end

    it "should be able to have more than one role assigned" do
      a = KubernetesSubject.new @service_account_a
      double_role_bindings = @role_bindings.map { |x| x }
      double_role_bindings.push(YAML.load_file(File.expand_path("spec/fixtures/bind_role_b_to_account_a.yaml")))
      a.define_roles @roles, double_role_bindings
      expect(a.roles.length).to be > 1
    end

    it "should properly detect if using a predefined role ruleset" do
      a = KubernetesSubject.new @service_account_a
      predefined_role_bindings = @role_bindings.map { |x| x }
      predefined_role_bindings.push(YAML.load_file(File.expand_path("spec/fixtures/bind_role_c_to_account_a.yaml")))
      a.define_roles @roles, predefined_role_bindings
      expect(a.roles.select(&:predefined).length).to eq(1)
    end
  end

  context "when defining the property representing the subject name" do
    it "should raise an error when not passed a Hash" do
      a = KubernetesSubject.new @service_account_a
      expect { a.set_name_property([]) }.to raise_error(ArgumentError)
    end
    it "should read and load the property configuration" do
      a = KubernetesSubject.new @service_account_a
      a.set_name_property(@name_mappings)
      expect(a.subject_name_property).to eq(@name_mappings[a.subject_name])
    end
  end

  context "when structuring output" do
    it "should reject any Kubernetes subject kind outer than ServiceAccount" do
      user_account = YAML.load_file(File.expand_path("spec/fixtures/user_account.yaml"))
      a = KubernetesSubject.new user_account
      expect { a.structure }.to raise_error(NotImplementedError)
    end
  end
end
