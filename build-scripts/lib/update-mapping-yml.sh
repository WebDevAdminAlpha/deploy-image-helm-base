#!/usr/bin/env bash
# update-mapping-yml
# Script to update the APP INSTANCE NAME in the mapping file

set -e

GL_MP_ENV="$(git rev-parse --show-toplevel)"
GL_MP_ENV_CONFIG="${GL_MP_ENV}/.gitlab_gke_marketplace_build_env"

[ ! -f "${GL_MP_ENV_CONFIG}" ] && echo "Missing ${GL_MP_ENV_CONFIG}" && exit 1

# shellcheck source=/dev/null
. "${GL_MP_ENV_CONFIG}"

[ -f "${GL_MP_SA_MAPPING_YAML}" ] || display_failure "${GL_MP_SA_MAPPING_YAML} missing!"

[ -z "${GL_MP_APP_INSTANCE_NAME}" ] && display_failure "GL_MP_APP_INSTANCE_NAME is not defined in the environment."

pattern="s/\$GL_MP_APP_INSTANCE_NAME/${GL_MP_APP_INSTANCE_NAME}/"

add_dead_file "${GL_MP_SA_MAPPING_YAML}-e"
sed -i -e "${pattern}" "${GL_MP_SA_MAPPING_YAML}" || display_failure "Insertion of ${GL_MP_APP_INSTANCE_NAME} failed"
